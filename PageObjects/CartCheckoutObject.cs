﻿using AventStack.ExtentReports;
using com.AutomationTestingProject.CommonFunctions;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.AutomationTestingProject.PageObjects
{
	public class CartCheckoutObject : BaseTestCase
	{
		

			[FindsBy(How = How.XPath, Using = "//input[@id='search_query_top']")]
			public IWebElement txtProduct;

			[FindsBy(How = How.ClassName, Using = "product-container")]
			public IList<IWebElement> searchProduct;

			[FindsBy(How = How.ClassName, Using = "ajax_add_to_cart_button")]
			public IWebElement clickToCart;

			[FindsBy(How = How.XPath, Using = "//a[@title='Proceed to checkout']")]
			public IWebElement proceedToCheckout;

			[FindsBy(How = How.XPath, Using = "//div[@id='layer_cart']//h2[1]")]
			public IWebElement verifyCartProduct;

		




	}

}

﻿using AventStack.ExtentReports;
using com.AutomationTestingProject.CommonFunctions;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.AutomationTestingProject.PageObjects
{
   public class LoginPage
    {



        [FindsBy(How = How.XPath, Using = "//input[@id='email']")]
        public IWebElement txtEmail;

        [FindsBy(How = How.XPath, Using = "//input[@id='passwd']")]
        public IWebElement txtPassword;

        [FindsBy(How = How.XPath, Using = "//button[@id='SubmitLogin']")]
        public IWebElement btnSubmit;


        public IWebDriver driver;

        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void enterUsername(string value)
        {
            txtEmail.SendKeys(value);
          

        }

        public void enterPassword(string value)
        {
            txtPassword.SendKeys(value);
            
        }

        public void clickSubmitLoginButton()
        {
            btnSubmit.Click();
        }

       /* public  void Login(string username, string password)
        {
            
            enterUsername(username);
            enterPassword(password);
            clickSubmitLoginButton();
        }
*/
      
    }


}

﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.AutomationTestingProject.CommonFunctions;

namespace com.AutomationTestingProject.PageObjects
{
   public  class UserFilterObjects
    {

        [FindsBy(How = How.XPath, Using = "//input[@id='txtUsername']")]
        public IWebElement LoginEmail;

        [FindsBy(How = How.XPath, Using = "//input[@id='txtPassword']")]
        public IWebElement LoginPassword;

        [FindsBy(How = How.XPath, Using = "//input[@id='btnLogin']")]
        public IWebElement clickOnbtn;

        [FindsBy(How = How.XPath, Using = "//li[@class='main-menu-first-level-list-item']/a[@id='menu_pim_viewPimModule']")]
        public IWebElement MainMenu;

        [FindsBy(How = How.XPath, Using = "//a[@id='menu_pim_viewEmployeeList']")]
        public IWebElement clickOnSubmenu;

        [FindsBy(How = How.XPath, Using ="//input[@id='empsearch_employee_name_empName']")]
        public IWebElement EmployeeName;

        [FindsBy(How = How.XPath, Using = "//input[@id='searchBtn']")]
        public IWebElement clickOnSearchbtn;

        [FindsBy(How = How.Id, Using = "empsearch_employee_status")]
        public IWebElement dropdownelement;


   



        public IWebDriver driver;

        public UserFilterObjects(IWebDriver driver) {
            this.driver = driver;
            PageFactory.InitElements(driver, this);

        }

       
    }
}

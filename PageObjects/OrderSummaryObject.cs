﻿using com.AutomationTestingProject.CommonFunctions;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.AutomationTestingProject.PageObjects
{
    public class OrderSummaryObject : BaseTestCase
    {
        [FindsBy(How = How.XPath, Using = "//a[text()='Printed Dress']")]
        public IWebElement AddedProduct;
    }
}

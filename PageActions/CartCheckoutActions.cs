﻿using AventStack.ExtentReports;
using com.AutomationTestingProject.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.AutomationTestingProject.PageActions
{
    public class CartCheckoutActions : CartCheckoutObject
    {
		public IWebDriver driver;

		public CartCheckoutActions(IWebDriver driver)
        {
            this.driver = driver;
			PageFactory.InitElements(driver, this);
		}
		
		public void enterProduct(string value)
        {
            txtProduct.SendKeys(value);
        }

		public void cartcheckout(string productType, ExtentTest report)
		{
          
			enterProduct(productType + Keys.Enter);
			report.Log(Status.Info, "Entered the product");
			report.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));
			CommonFunctions.BaseTestCase.pageScroll(350);
		}

		public void productContainer(string value, ExtentTest report)
		{
			IList<IWebElement> products = searchProduct;
			var displayedProducts = products.Where(it => it.Displayed);

			if (displayedProducts.Count() <= 0)
			{
				throw new Exception("No Products available");
			}
			else
			{
				foreach (IWebElement item in displayedProducts)
				{
					if (item.Text.Contains(value))
					{
							report.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));
							report.Log(Status.Info, "Searched product is:" + value);
							CommonFunctions.BaseTestCase.elementToScroll(item);
							CommonFunctions.BaseTestCase.moveToelement(item);
							System.Threading.Thread.Sleep(1000);
							CartCheckoutActions page = new CartCheckoutActions(driver);
							page.clickToCart.Click();
							System.Threading.Thread.Sleep(2000);
							report.Log(Status.Info, "Added to the  Product Checkout");
							report.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));

					}
				}
			}

            try
            {
				
				Assert.IsTrue(verifyCartProduct.Displayed);
				report.Log(Status.Pass, "Product Checked out");
				report.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));

			}
			catch (AssertFailedException afe){

				report.Log(Status.Fail, afe.Message);
				report.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));
			}



			}
		public void ProceedToCheckout(ExtentTest report)
		{
			
			proceedToCheckout.Click();
			report.Log(Status.Info, "Clicked on the Proceed to Checkout");
			report.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));

		}



	}
}

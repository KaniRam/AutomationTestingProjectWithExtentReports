﻿using AventStack.ExtentReports;
using com.AutomationTestingProject.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.AutomationTestingProject.PageActions
{
    public class OrderSummaryActions : OrderSummaryObject
    {
        public IWebDriver driver;

        public OrderSummaryActions(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void VerificationOnOrderSummary(ExtentTest report)
        {
            IWebElement actualMessage = AddedProduct;
            Assert.IsNotNull(actualMessage);
            report.Log(Status.Info, "Listing the product in the order summary");
            report.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));


        }
    }
}

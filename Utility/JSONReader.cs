﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.AutomationTestingProject.Utility
{
     class JSONReader
    {

        public JObject jsonFileObject;
        public static string testDataDir = "TestData";
        public string jstring;
        public void readJson(string fileName)
        {
            var path = Path.Combine(Directory.GetParent(".").FullName, "..", testDataDir, fileName);
            // read JSON directly from a file
            StreamReader file = File.OpenText(Path.GetFullPath(path));
            jstring = File.ReadAllText(path);

            JsonTextReader reader = new JsonTextReader(file);
            jsonFileObject = (JObject)JToken.ReadFrom(reader);
        }

        public string getValueForKey(object key) {
            var value = jsonFileObject[key].ToString();
            return value;
        }

        public dynamic getDynamicData()
        {
            return JsonConvert.DeserializeObject<dynamic>(jstring);

        }



    }
    
}

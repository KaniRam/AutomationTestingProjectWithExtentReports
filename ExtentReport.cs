﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.AutomationTestingProject
{
   public class ExtentReport
    {
        public static ExtentReports extent;
        public static ExtentHtmlReporter htmlReporter;
        public static string reportDir = "";

        public static ExtentReports getReportInstance()
        {
           
                string currentTime = DateTime.Now.ToString("yyyy_dd_MM_HH_mm_ss");               
                var projectPath = Path.Combine(Directory.GetParent(".").FullName,"..");

                reportDir = "ReportFolder - "+ currentTime;
                var reportDirFullPath = Path.GetFullPath(Path.Combine(projectPath, "ReportFolder", reportDir));
                if (!Directory.Exists(reportDirFullPath)) {
                    Directory.CreateDirectory(reportDirFullPath);
                } 
                var reportPath = Path.GetFullPath(Path.Combine(reportDirFullPath, "Report.html"));
                var configPath = Path.GetFullPath(Path.Combine(projectPath, "Extent-Config.xml"));

                htmlReporter = new ExtentHtmlReporter(reportPath, ViewStyle.SPA);
                htmlReporter.Config.Theme = Theme.Standard;
                extent = new ExtentReports();
                extent.AttachReporter(htmlReporter);
      
                extent.AddSystemInfo("OS", Environment.OSVersion.ToString());
                extent.AddSystemInfo("Machine Name", Environment.MachineName);
                extent.AddSystemInfo("User Name", Environment.UserName);
                extent.AddSystemInfo("Domain Name", Environment.UserDomainName);
                htmlReporter.LoadConfig(configPath);
                    
            
            return extent;
        } 

    }
}

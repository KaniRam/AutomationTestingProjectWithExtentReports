﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.AutomationTestingProject.JSONConfiguration
{
    public class CartCheckoutConfig
    {
        public string URL { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Employeename { get; set; }
        public string Employeestatus { get; set; }
    }
}

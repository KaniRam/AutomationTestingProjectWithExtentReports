﻿using AventStack.ExtentReports;
using com.AutomationTestingProject.CommonFunctions;
using com.AutomationTestingProject.PageActions;
using com.AutomationTestingProject.PageObjects;
using com.AutomationTestingProject.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using OpenQA.Selenium.Support.UI;

namespace com.AutomationTestingProject.TestCases
{
    [TestClass]
    
    public class CartCheckoutTestcase : BaseTestCase
    {
        JSONReader jsonData;
        

        [TestCleanup]
        public void testCleanUp()
        {
            BaseTestCase.closeBrowser();
            BaseTestCase.getLog.Flush();
        }

        public void loadJson() {
            if (jsonData == null) {
                jsonData = new JSONReader();
                jsonData.readJson("AddToCart.json");
            }
        }

        [TestMethod, TestCategory("CategoryA")]
        public  void CheckOutProduct()
        {
            string title = "Adding into Cart";
            try
            {
                loadJson();
                AddToCartConfig jdata = JsonConvert.DeserializeObject<AddToCartConfig>(jsonData.jstring);
                
                BaseTestCase.Startup(jdata.URL, "CheckOutProduct");

                var loginPage = new LoginPage(getDriver);
                loginPage.enterUsername(jdata.Username);
                loginPage.enterPassword(jdata.Password);

                var cartpageaction = new CartCheckoutActions(getDriver);
                cartpageaction.cartcheckout(jdata.ProductSearch, getReport);
                cartpageaction.productContainer(jdata.ProductType, getReport);
                cartpageaction.ProceedToCheckout(getReport);

                var ordersummaryaction = new OrderSummaryActions(getDriver);
                ordersummaryaction.VerificationOnOrderSummary(getReport);


            }
            catch (Exception cartCheckoutException)
            {
                BaseTestCase.HandleException(title, cartCheckoutException);
               }

        }
    }
}
﻿using com.AutomationTestingProject.CommonFunctions;
using com.AutomationTestingProject.JSONConfiguration;
using com.AutomationTestingProject.PageObjects;
using com.AutomationTestingProject.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.AutomationTestingProject.TestCases
{
    [TestClass ]
 
   
    public class UserFilterTestcase : BaseTestCase
    {
        JSONReader jsonData;

        [TestCleanup]
        public void testCleanUp()
        {
            BaseTestCase.closeBrowser();
            BaseTestCase.getLog.Flush();
        }

        public void loadJson()
        {
            if (jsonData == null)
            {
                jsonData = new JSONReader();
                jsonData.readJson("UserFilter.json");
            }
        }

        [TestMethod]
        public void UserSearchFilter()
        {
            string title = "User Filter Search";
            try
            {
                loadJson();
                CartCheckoutConfig jdata = JsonConvert.DeserializeObject<CartCheckoutConfig>(jsonData.jstring);

                BaseTestCase.Startup(jsonData.getValueForKey("URL"), "UserSearchFilter");

                var userFliter = new UserFilterObjects(getDriver);
                BaseTestCase.EnterText(userFliter.LoginEmail, jdata.Username);
                BaseTestCase.EnterText(userFliter.LoginPassword, jdata.Password);
                BaseTestCase.ClickOnElement(userFliter.clickOnbtn);
                BaseTestCase.moveToelement(userFliter.MainMenu);
                BaseTestCase.ClickOnElement(userFliter.clickOnSubmenu);
                BaseTestCase.EnterText(userFliter.EmployeeName, jdata.Employeename);
       
                //BaseTestCase.DropDownElement(userFliter.dropdownelement, jsonData.getValueForKey("Employeestatus"));
                BaseTestCase.ClickOnElement(userFliter.clickOnSearchbtn);         
                BaseTestCase.HandlingWebTable(jdata.Employeename);

                
            }
            catch (Exception UserSearchFilter)
            {
                BaseTestCase.HandleException(title, UserSearchFilter);
            }

        }
    
    }
}

﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using System.IO;

namespace com.AutomationTestingProject
{
    public class getScreenshot
    {
        
        public static string screenshotDir = "Screenshots";
        public static string  takeScreenshot(IWebDriver driver)
        {
            var projectPath = Path.Combine(Directory.GetParent(".").FullName, "..");
            var screenshotPath = Path.GetFullPath(Path.Combine(projectPath, "ReportFolder", ExtentReport.reportDir, screenshotDir));

            if (!Directory.Exists(screenshotPath))
            {
                Directory.CreateDirectory(screenshotPath);
            }
            ITakesScreenshot ts = (ITakesScreenshot)driver;
            Screenshot sc = ts.GetScreenshot();
            string imageName = DateTime.Now.ToString("ddMMyyyyHHmmssffff") + ".png";
            var imageFullPath = Path.GetFullPath(Path.Combine(screenshotPath,imageName));
            sc.SaveAsFile(imageFullPath, ScreenshotImageFormat.Png);
            return new Uri(imageFullPath).LocalPath;
        }
        
    }
}

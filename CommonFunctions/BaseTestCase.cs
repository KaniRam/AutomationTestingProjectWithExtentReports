﻿using AventStack.ExtentReports;
using com.AutomationTestingProject.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace com.AutomationTestingProject.CommonFunctions
{
    [TestClass]
    public class BaseTestCase : ExtentReport
    {

        private static IWebDriver driver;
        private static ExtentReports log = ExtentReport.getReportInstance();
        private static ExtentTest report;

        public static IWebDriver getDriver
        {
            get { return driver; }
            set { driver = value; }
        }

        public static ExtentReports getLog
        {
            get { return log; }
            set { log = value; }
        }

        public static ExtentTest getReport
        {
            get { return report; }
            set { report = value; }
        }


        public static void Startup(string URL, string reportName)
        {
            getDriver = new ChromeDriver();
            getReport = getLog.CreateTest(reportName);

            getReport.Log(Status.Info, "Chrome browser opened");
            getReport.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));

            getDriver.Navigate().GoToUrl(URL);
            MaximizeWindow();
            getReport.Log(Status.Info, "Navigated to the URL:");
            getReport.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));

        }

        public static void closeBrowser()
        {
            if (getDriver != null)
            {
                getDriver.Close();
            }

        }

        public void Refresh()
        {
            getDriver.Navigate().Refresh();
        }

        public static WebDriverWait Wait()
        {
            return new WebDriverWait(getDriver, TimeSpan.FromSeconds(10));
        }

        public static WebDriverWait Wait(double seconds)
        {
            return new WebDriverWait(getDriver, TimeSpan.FromSeconds(seconds));
        }
        public static void MaximizeWindow()
        {
            getDriver.Manage().Window.Maximize();
        }

        public static string Title()
        {
            try
            {
                return "" + getDriver.Title;
            }
            catch
            {
                return string.Empty;
            }
        }
        public static void ValidateTitle(string title, string failureErrorMsg)
        {
            try
            {
                if (!Title().ToUpper().Equals(title.ToUpper()))
                {
                    throw new Exception(failureErrorMsg);
                }
            }
            catch
            {
                throw new Exception(failureErrorMsg);
            }
        }
        public static void elementToScroll(IWebElement value)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].scrollIntoView();", value);
        }

        public static void pageScroll(int num)
        {

            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
            jse.ExecuteScript("window.scrollBy(0," + num.ToString() + ")", "");

        }
        public static void moveToelement(IWebElement value)
        {
            Actions actions = new Actions(getDriver);
            actions.MoveToElement(value).Perform();
            actions.Build();
        }


        public static void WaitFor(IWebElement element)
        {
            try
            {
                Wait().Until(e => element);
                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public static void HandleException(string title, Exception e)
        {

            getReport.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(getDriver), title + " failed : " + e.Message);
            getReport.Log(Status.Info, "Exception Captured :" + title + " failed : " + e.Message);



        }

        public static void EnterText(IWebElement element, String strValue)
        {
            if (!strValue.Equals("<::blank::>"))
            {
                try
                {
                    element.Click();
                    element.Clear();
                    if (!strValue.Equals("<::empty::>"))
                        element.SendKeys(strValue);
                    getReport.Log(Status.Info, "Entered the value:" + strValue);
                    getReport.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
        }

        public static void ClickOnElement(IWebElement element)
        {
            WaitFor(element);
            if (element.Enabled)
            {
                try
                {
                    element.Click();
                    getReport.Log(Status.Info, "Click on the button:" + element.Text);
                    getReport.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));
                }

                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

        }

        public static void DropDownElement(IWebElement element, string strvalue)
        {
            var selectElement = new SelectElement(element);
            if (element.GetAttribute("value") != null && element.GetAttribute("value") == strvalue)
            {
                selectElement.SelectByValue(strvalue);
            }
            else if (element.Text == strvalue)
            {
                selectElement.SelectByText(strvalue);
            }
            else if (Int32.TryParse(strvalue, out int numValue))
            {
                selectElement.SelectByIndex(numValue);
            }

        }

        public static void HandlingWebTable(string strvalue)
        {

            IList<IWebElement> column = driver.FindElements(By.XPath("//tr"));

            IList<IWebElement> row = driver.FindElements(By.XPath("//td"));

            IWebElement tablevalue = driver.FindElement(By.XPath("//td[normalize-space()='" + strvalue + "']"));

            if (tablevalue != null)
            {

                tablevalue.FindElement(By.TagName("a")).Click();
                getReport.Log(Status.Info, "Entered the value:" + tablevalue.Text);
                getReport.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));
            }
            else
            {
                throw new Exception();
            }
        }

        public static void HandlingWebTable1(string strvalue)
        {

            IList<IWebElement> column = driver.FindElements(By.XPath("//tr"));
            int columncount = column.Count();
            IList<IWebElement> row = driver.FindElements(By.XPath("//td"));
            int rowcount = column.Count();

            IWebElement tablevalue = driver.FindElement(By.XPath("//td[normalize-space()='" + strvalue + "']"));
          
            if (tablevalue != null)
            {

                tablevalue.FindElement(By.TagName("a")).Click();
                getReport.Log(Status.Info, "Entered the value:" + tablevalue.Text);
                getReport.AddScreenCaptureFromPath(getScreenshot.takeScreenshot(driver));
            }
            else
            {
                throw new Exception();
            }
        }

       
    
    

}
}
